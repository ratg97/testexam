# **Proyecto de Eclipse** #

Un ejemplo de un examen tipo test.
Recoge de un XML las x preguntas y una vez realizado el examen genera un PDF con el resultado.

![ewe.PNG](https://bitbucket.org/repo/ekkr8Mp/images/1873336949-ewe.PNG)
![we.PNG](https://bitbucket.org/repo/ekkr8Mp/images/2157908610-we.PNG)

# **Metodología:** #

1. No Swing

1. Todo aleatorio

1. MVC

1. Constantes

1. Expresiones regulares

1. Librería XML: jdom-2.0.6.jar 

1. Librería PDF: itextpdf-5.5.3.jar