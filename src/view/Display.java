package view;

public class Display {
	private Display() {
	}
	
	public static void writeln(String s){
		System.out.println(s);
	}
	public static void write(String s){
		System.out.print(s);
	}
}
