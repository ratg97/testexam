package main;

import controller.ExamController;
import controller.ListController;
import controller.SettingsController;
import controller.StudentController;

public class Main {

	public static void main(String[] args) {
		ListController list = new ListController();
		list.setQuestionsFromXMLFile();
		SettingsController settings = new SettingsController(list);
		settings.setSettingsRight();
		StudentController student = new StudentController();
		student.setStudentInformation();
		ExamController exam = new ExamController(settings, student);
		exam.start();
	}
}
