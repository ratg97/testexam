package utils;

import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import view.Display;

public class PreExamUtils {
	
	private PreExamUtils() {
	}

	public static String askName() {
		String name;
		do {
			Display.write("Introduce tu nombre " + Constants.REGEX_NAME_STUDENT + ": ");
			name = new Scanner((new InputStreamReader(System.in, Charset.forName("ISO-8859-1")))).nextLine();
			if (!isName(name)) {
				Display.writeln("\n--> Introduce un nombre correcto\n");
			}
		} while (!isName(name));
		return name;
	}

	private static boolean isName(String word) {
		Pattern pat = Pattern.compile(Constants.REGEX_NAME_STUDENT);
		Matcher mat = pat.matcher(word);
		return mat.matches();
	}
}
