package utils;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;

public class PDFHeaderFooter extends PdfPageEventHelper {

	private final static String END_MESSAGE = "� Examen tipo Test - Creado por Ra�l TG";
	private final static int NUM_COLUM_TABLE = 2;
	private final static int BORDER_WITH_TOP = 3;

	@Override
	public void onEndPage(final PdfWriter writer, final Document document) {
		PdfPTable table = new PdfPTable(NUM_COLUM_TABLE);
		table.setTotalWidth(document.getPageSize().getWidth() - document.rightMargin() - document.leftMargin());
		table.addCell(getCellFooter(new Paragraph(END_MESSAGE),Element.ALIGN_LEFT, BaseColor.BLACK, BORDER_WITH_TOP));
		table.addCell(getCellFooter(new Paragraph(String.format(" %d", writer.getPageNumber())),Element.ALIGN_RIGHT, BaseColor.BLACK, BORDER_WITH_TOP));
		table.writeSelectedRows(0, -1, 36, 64, writer.getDirectContent());
	}
	
	private static PdfPCell getCellFooter(Paragraph p, int e, BaseColor c, int s ){
		PdfPCell customCell = new PdfPCell(p);
		customCell.setHorizontalAlignment(e);
		customCell.setBorderColor(BaseColor.WHITE);
		customCell.setBorderColorTop(c);
		customCell.setBorderWidthTop(s);
		return customCell;
	}
}
