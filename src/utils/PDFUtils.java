package utils;

import java.io.FileOutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;

import com.itextpdf.text.Paragraph;

import com.itextpdf.text.Rectangle;

import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import model.Question;
import model.Student;
import view.Display;

public class PDFUtils {

	public static final String NO_ANSWER_SQUARE = "[  ] ";

	public static final String ANSWER_SQUARE = "[X] ";

	private static final Font TITLE_QUESTION = new Font(
			FontFactory.getFont("Arial", Font.DEFAULTSIZE, Font.BOLD, BaseColor.BLACK));

	private static final Font RIGHT_ANSWER = new Font(
			FontFactory.getFont("Arial", Font.DEFAULTSIZE, Font.UNDERLINE, BaseColor.GREEN));

	private static final Font WRONG_ANSWER = new Font(
			FontFactory.getFont("Arial", Font.DEFAULTSIZE, Font.STRIKETHRU, BaseColor.RED));

	private static final Font EMPTY_ANSWER = new Font(
			FontFactory.getFont("Arial", Font.DEFAULTSIZE, Font.NORMAL, BaseColor.BLACK));

	private static final int NUM_COLUMN_TABLE = 4;
	private static final float WIDTH_ALL_TABLE = 100f;
	private static final float SPACING_BETWEEN_TABLE = 25f;

	private PDFUtils() {
	}

	public static void write(Student student, String[] solutionsStudent, ArrayList<Question> questionsFromXMLFile,
			int size) {
		PdfWriter writer = null;
		try {
			Document document = new Document();
			writer = PdfWriter.getInstance(document, new FileOutputStream(Constants.RESULT));

			PDFHeaderFooter event = new PDFHeaderFooter();
			writer.setPageEvent(event);

			document.open();

			//Valor de cada acierto, fallada, en blanco
			double gradePerRight = getRoundDouble(((double)Constants.MAX_GRADE / (double)size));
			double failurePenalty = getRoundDouble((gradePerRight / 2.0));
			//Contadores
			int rightAnswer = 0;
			int wrongAnswer = 0;
			int emptyAnswer = 0;

			PdfPTable table = new PdfPTable(NUM_COLUMN_TABLE);
			table.setWidthPercentage(WIDTH_ALL_TABLE);
			float[] columnWidths = new float[] { 15f, 30f, 40f, 15f };
			table.setWidths(columnWidths);
			PdfPCell cellHeader;

			Image imagen = Image.getInstance("Ribera.gif");
			Paragraph imageP = new Paragraph();
			cellHeader = new PdfPCell();
			int scale = 36;
			imagen.scalePercent(scale);
			imageP.add(new Chunk(imagen, 0, 0, true));
			cellHeader.setRowspan((int) NUM_COLUMN_TABLE);
			cellHeader.addElement(imageP);
			cellHeader.setBorder(Rectangle.NO_BORDER);
			table.addCell(cellHeader);

			Paragraph course = new Paragraph();
			course.add(new Chunk("Curso: ", TITLE_QUESTION));
			course.add(new Chunk("1ºDAW"));
			cellHeader = new PdfPCell(course);
			cellHeader.setColspan(((int) (NUM_COLUMN_TABLE)) - 1);
			table.addCell(cellHeader);

			Paragraph subject = new Paragraph();
			subject.add(new Chunk("Asignatura: ", TITLE_QUESTION));
			subject.add(new Chunk("Marcas"));
			cellHeader = new PdfPCell(subject);
			cellHeader.setColspan(((int) (NUM_COLUMN_TABLE)) - 1);
			table.addCell(cellHeader);

			Paragraph nameStudent = new Paragraph();
			nameStudent.add(new Chunk("Alumno: ".toUpperCase(), TITLE_QUESTION));
			nameStudent.add(new Chunk(student.getName()));
			cellHeader = new PdfPCell();
			cellHeader.addElement(nameStudent);
			cellHeader.setColspan(((int) (NUM_COLUMN_TABLE)) - 2);
			table.addCell(cellHeader);

			Paragraph gradeP = new Paragraph();
			gradeP.add(new Chunk("Calificación", TITLE_QUESTION));
			gradeP.setAlignment(Paragraph.ALIGN_CENTER);
			cellHeader = new PdfPCell();
			cellHeader.setBackgroundColor(BaseColor.GRAY);
			cellHeader.addElement(gradeP);

			table.addCell(cellHeader);

			cellHeader = new PdfPCell();
			Paragraph evaP = new Paragraph();
			evaP.add(new Chunk("3ºEvaluación".toUpperCase(), TITLE_QUESTION));
			cellHeader.addElement(evaP);
			Paragraph testP = new Paragraph("Examen tipo Test");
			cellHeader.addElement(testP);
			table.addCell(cellHeader);

			cellHeader = new PdfPCell();
			Date today = new Date();
			SimpleDateFormat formato = new SimpleDateFormat("dd 'de 'MMMM 'de 'yyyy");
			String todayS = formato.format(today);
			Paragraph dateP = new Paragraph("Fecha: " + todayS);
			dateP.setAlignment(Paragraph.ALIGN_CENTER);
			cellHeader.addElement(dateP);
			table.addCell(cellHeader);

			PdfPTable tableQuestions = new PdfPTable(NUM_COLUMN_TABLE - 2);
			tableQuestions.setWidthPercentage(WIDTH_ALL_TABLE);
			tableQuestions.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
			

			for (int i = 0; i < size; i++) {
				Paragraph name = getParagraph((i + 1) + ".  ", questionsFromXMLFile.get(i).getName(), TITLE_QUESTION);
				
				PdfPCell cell = new PdfPCell();
				cell.setBorder(Rectangle.NO_BORDER);
				if (i % 2 != 0) {
					cell.setPaddingLeft(document.leftMargin());
				}

				cell.addElement(name);

				int len = questionsFromXMLFile.get(i).getAnswers().length;
				if (checkAnswer(solutionsStudent[i], questionsFromXMLFile.get(i).getSolution())) {
					rightAnswer++;
					for (int j = 0; j < len; j++) {
						if (solutionsStudent[i].equalsIgnoreCase(questionsFromXMLFile.get(i).getAnswers()[j])) {
							Paragraph p = getParagraph(ANSWER_SQUARE, questionsFromXMLFile.get(i).getAnswers()[j],
									RIGHT_ANSWER);
							cell.addElement(p);

						} else {
							Paragraph answer = getParagraph(NO_ANSWER_SQUARE,
									questionsFromXMLFile.get(i).getAnswers()[j], EMPTY_ANSWER);
							cell.addElement(answer);

						}
					}
				} else {
					if (solutionsStudent[i].equalsIgnoreCase("")) {
						emptyAnswer++;
						for (int j = 0; j < len; j++) {
							if (questionsFromXMLFile.get(i).getAnswers()[j]
									.equalsIgnoreCase(questionsFromXMLFile.get(i).getSolution())) {
								Paragraph solution = getParagraph(NO_ANSWER_SQUARE,
										questionsFromXMLFile.get(i).getAnswers()[j], RIGHT_ANSWER);
								cell.addElement(solution);

							} else {
								Paragraph answer = getParagraph(NO_ANSWER_SQUARE,
										questionsFromXMLFile.get(i).getAnswers()[j], EMPTY_ANSWER);
								cell.addElement(answer);

							}
						}
					} else {
						wrongAnswer++;
						for (int j = 0; j < len; j++) {
							if (solutionsStudent[i].equalsIgnoreCase(questionsFromXMLFile.get(i).getAnswers()[j])) {
								Paragraph solutionStudent = getParagraph(ANSWER_SQUARE,
										questionsFromXMLFile.get(i).getAnswers()[j], WRONG_ANSWER);
								cell.addElement(solutionStudent);

							} else {
								if (questionsFromXMLFile.get(i).getAnswers()[j]
										.equalsIgnoreCase(questionsFromXMLFile.get(i).getSolution())) {
									Paragraph solution = getParagraph(NO_ANSWER_SQUARE,
											questionsFromXMLFile.get(i).getAnswers()[j], RIGHT_ANSWER);
									cell.addElement(solution);

								} else {
									Paragraph answer = getParagraph(NO_ANSWER_SQUARE,
											questionsFromXMLFile.get(i).getAnswers()[j], EMPTY_ANSWER);
									cell.addElement(answer);

								}
							}
						}
					}
				}

				tableQuestions.addCell(cell);
			}

			tableQuestions.setSpacingBefore(SPACING_BETWEEN_TABLE);
			tableQuestions.setSpacingAfter(SPACING_BETWEEN_TABLE);

			//Calculamos la nota...
			double gradePerRightTotal = getRoundDouble(gradePerRight * rightAnswer);
			double gradePerWrongTotal = getRoundDouble(wrongAnswer * failurePenalty);
			double gradePerEmptyTotal = getRoundDouble(emptyAnswer * Constants.EMPTY_QUESTION_VALUE);
			double gradeTotal = getRoundDouble(gradePerRightTotal - gradePerWrongTotal + gradePerEmptyTotal);
			student.setGrade(gradeTotal);

			Paragraph gradeH = new Paragraph("" + ((student.getGrade() % 2 == 0.0)
					? (new DecimalFormat("##").format(student.getGrade())) : student.getGrade()));
			gradeH.setAlignment(Paragraph.ALIGN_CENTER);
			gradeH.setFont(new Font(FontFactory.getFont("Arial", 18, Font.BOLD, BaseColor.BLACK)));
			cellHeader = new PdfPCell();
			cellHeader.addElement(gradeH);
			table.addCell(cellHeader);

			table.completeRow();
			tableQuestions.completeRow();

			document.add(table);
			document.add(tableQuestions);

			PdfPTable tableResult = new PdfPTable(NUM_COLUMN_TABLE);
			tableResult.setWidthPercentage(WIDTH_ALL_TABLE);
			//tableResult.getDefaultCell().setBorder(PdfPCell.NO_BORDER);

			Paragraph rightAnswerT = new Paragraph(
					"Acertadas:\n" + rightAnswer + " x " + gradePerRight + " = " + gradePerRightTotal);
			//No hace falta crear celdas para añadir párrafos en una tabla...
			//Es compatible con: tableResult.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
			tableResult.addCell(rightAnswerT);

			Paragraph wrongAnswerT = new Paragraph(
					"Falladas:\n" + wrongAnswer + " x " + failurePenalty + " = " + gradePerWrongTotal);
			tableResult.addCell(wrongAnswerT);

			Paragraph emptyAnswerT = new Paragraph(
					"En blanco:\n" + emptyAnswer + " x " + 0 + " = " + gradePerEmptyTotal);
			tableResult.addCell(emptyAnswerT);

			Paragraph grade = new Paragraph("Nota final: " + ((student.getGrade() % 2 == 0.0)
					? (new DecimalFormat("##").format(student.getGrade())) : student.getGrade()));
			tableResult.addCell(grade);
			
			tableResult.completeRow();

			document.add(tableResult);

			document.close();
			writer.close();
		} catch (Exception ex) {
			ex.getMessage();
		} finally {
			if (writer != null) {
				writer.close();
			}
		}
	}

	private static boolean checkAnswer(String answerStudent, String solutionQuestion) {
		return (solutionQuestion.equalsIgnoreCase(answerStudent));
	}

	private static Paragraph getParagraph(String leftIcon, String answer, Font rightAnswer) throws Exception {
		Paragraph p = new Paragraph();
		p.add(new Chunk(leftIcon));
		p.add(new Chunk(answer, rightAnswer));
		p.setAlignment(Element.ALIGN_JUSTIFIED);
		return p;
	}
	
	private static double getRoundDouble(double d){
		return Math.round(d*100.0)/100.0;
	}

}
