package utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import model.Question;
import view.Display;

public class XMLUtils {

	private XMLUtils() {

	}

	public static void read(ArrayList<Question> questionsFromXMLFile) {
		SAXBuilder builder = new SAXBuilder();
		if (Constants.EXAM.exists()) {
			try {
				Document document = (Document) builder.build(Constants.EXAM);

				Element rootNode = document.getRootElement();
				boolean firstTime = true;
				List<Element> questionsList = rootNode.getChildren("question");
				for (int i = 0; i < questionsList.size(); i++) {
					Element questionsNode = questionsList.get(i);
					String name = questionsNode.getChildText("name");
					String[] answersArray = new String[3];
					for (int z = 0; z < answersArray.length; z++) {
						answersArray[z] = questionsNode.getChildText("answer" + (z + 1));
					}
					String solution = questionsNode.getChildText("solution");
					int z = 0;
					boolean encontrado = false;
					while (!encontrado && z < answersArray.length) {
						if (solution.equalsIgnoreCase(answersArray[z])) {
							encontrado = true;
						}
						z++;
					}
					if (!encontrado) {
						Display.writeln("Error al leer en la pregunta " + (i + 1)
								+ ": la soluci�n no se encuentra entre las respuestas posibles...");
						System.exit(0);
					}
					if (firstTime) {
						questionsFromXMLFile.add(new Question(name, answersArray, solution));
						firstTime = false;
					} else {
						Question q = new Question(name, answersArray, solution);
						if (!checkQuestionRepeat(questionsFromXMLFile, q)) {

							questionsFromXMLFile.add(q);
						} else {
							Display.writeln("Error al leer en la pregunta " + (i + 1) + ": est� repetida...");
							System.exit(0);
						}
					}
				}
			} catch (Exception io) {
				io.printStackTrace();
				Display.writeln(io.getMessage());
			}
		} else {
			Display.writeln("Error al leer: examen no encontrado...");
			System.exit(0);
		}
	}

	private static boolean checkQuestionRepeat(ArrayList<Question> questionsFromXMLFile, Question q) {
		for (Question que : questionsFromXMLFile) {
			if (que.getName().equalsIgnoreCase(q.getName())) {
				return true;
			}
		}
		return false;
	}

	public static void write(ArrayList<Question> questionsFromXMLFile) {
		try {
			int len;
			Element trivial = new Element("exam");
			Document doc = new Document(trivial);
			int size = questionsFromXMLFile.size();
			for (int i = 0; i < size; i++) {
				Element question = new Element("question");
				Element name = new Element("name").setText(questionsFromXMLFile.get(i).getName());
				question.addContent(name);
				len = questionsFromXMLFile.get(i).getAnswers().length;
				for (int x = 0; x < len; x++) {
					Element answer = new Element("answer" + (x + 1))
							.setText(questionsFromXMLFile.get(i).getAnswers()[x]);
					question.addContent(answer);
				}
				Element solution = new Element("solution").setText(questionsFromXMLFile.get(i).getSolution());
				question.addContent(solution);

				doc.getRootElement().addContent(question);
			}

			FileOutputStream fos = new FileOutputStream(Constants.EXAM);
			Format format = Format.getPrettyFormat();
			format.setEncoding("UTF-8");
			XMLOutputter outputter = new XMLOutputter(format);
			outputter.output(doc, fos);

		} catch (Exception e) {
			Display.writeln(e.getMessage());
		}
	}
}
