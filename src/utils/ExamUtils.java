package utils;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import view.Display;

public class ExamUtils {
	
	private ExamUtils() {
	}

	public static int askAnswer() {
		int number = Constants.LOOP_VALUE;
		boolean rigthAnswer;
		do {
			try {
				do {
					Display.write("--> Introduce tu respuesta " + Constants.REGEX_ANSWER_EXAM + ": ");
					number = new Scanner(System.in).nextInt();
					if (!isAnswer(number)) {
						Display.writeln("\nIntroduce un valor correcto\n");
					}
				} while (!isAnswer(number));
				rigthAnswer = true;
			} catch (final Exception e) {
				rigthAnswer = false;
				Display.writeln("\nIntroduce un valor correcto\n");
			}
		} while (!rigthAnswer);
		return number;
	}

	private static boolean isAnswer(int answer) {
		Pattern pat = Pattern.compile(Constants.REGEX_ANSWER_EXAM);
		Matcher mat = pat.matcher("" + answer);
		return mat.matches();
	}
}
