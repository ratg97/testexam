package utils;

import java.io.File;

public class Constants {

	private Constants() {
	}

	public static final int LOOP_VALUE = -1;

	public static final File EXAM = new File("exam.xml");

	public static final File RESULT = new File("result.pdf");

	public static final String REGEX_ANSWER_EXAM = "[0123]{1,1}";

	public static final String REGEX_NAME_STUDENT = "^[A-Z������][a-z�������]+{1,10}";

	public static final int MIN_NQUESTIONS_EXAM = 10;

	public static final int MAX_GRADE = 10;

	public static final int DEFAULT_GRADE_START_EXAM = 0;
	
	public static final int EMPTY_QUESTION_VALUE = 0;
}
