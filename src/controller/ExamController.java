package controller;

import utils.Constants;
import utils.ExamUtils;
import utils.PDFUtils;
import view.Display;

public class ExamController {

	private SettingsController settings;
	private StudentController student;

	public ExamController(SettingsController settings, StudentController student) {
		this.settings = settings;
		this.student = student;
	}

	public void start() {
		if (this.settings.isSettingsRight()) {
			// Siempre 10...si las hay
			int nQuestions = this.settings.getList().getQuestionsFromXMLFile().size() > Constants.MIN_NQUESTIONS_EXAM
					? Constants.MIN_NQUESTIONS_EXAM : this.settings.getList().getQuestionsFromXMLFile().size();
			// Si queremos ag�n n�mero en concreto
			// int nQuestions = 11;
			String[] solutionsStudent = new String[nQuestions];
			boolean finish = false;
			int i = 0;
			while (i < nQuestions && !finish) {
				Display.writeln("\nPreguntas restantes: " + (nQuestions - i));
				showQuestion(i);
				int answerInt = ExamUtils.askAnswer();
				// 0 es no responder
				if (answerInt != 0) {
					solutionsStudent[i] = this.settings.getList().getQuestionsFromXMLFile().get(i)
							.getAnswers()[answerInt - 1];
				} else {
					solutionsStudent[i] = "";
				}
				i++;
				if (!(i < nQuestions)) {
					finish = true;
				}
			}

			PDFUtils.write(this.student.getStudent(), solutionsStudent,
					this.settings.getList().getQuestionsFromXMLFile(), nQuestions);
		} else {
			Display.writeln("--> Error en la lista de preguntas: n� insuficiente...\n");
			System.exit(0);
		}
	}

	private void showQuestion(int i) {
		Display.writeln("\n" + this.settings.getList().getQuestionsFromXMLFile().get(i).getName());
		int x = 0;
		Display.writeln("\t" + (x) + ") " + "En blanco");
		for (String qS : this.settings.getList().getQuestionsFromXMLFile().get(i).getAnswers()) {
			x++;
			Display.writeln("\t" + x + ") " + qS);
		}

	}
}
