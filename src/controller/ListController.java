package controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import model.Question;
import utils.XMLUtils;

public class ListController {

	private ArrayList<Question> questionsFromXMLFile;

	public ListController() {
		this.questionsFromXMLFile = new ArrayList<Question>();
	}

	public void setQuestionsFromXMLFile() {
		XMLUtils.read(this.questionsFromXMLFile);
		// Aleatorio
		long seed = System.nanoTime();
		Collections.shuffle(this.questionsFromXMLFile, new Random(seed));
	}

	public ArrayList<Question> getQuestionsFromXMLFile() {
		return questionsFromXMLFile;
	}
}
