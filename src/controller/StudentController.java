package controller;

import model.Student;
import utils.Constants;
import utils.PreExamUtils;

public class StudentController {
	
	private Student student;

	public StudentController() {
		this.student = new Student();
	}

	public void setStudentInformation(){
		this.student.setName(PreExamUtils.askName());
		this.student.setGrade(Constants.DEFAULT_GRADE_START_EXAM);
	}

	public Student getStudent() {
		return student;
	}
}
