package model;

public class Question {
	
	private String name;
	private String[] answers;
	private String solution;
	
	public Question(String name, String[] answers, String solution) {
		super();
		this.name = name;
		this.answers = answers;
		this.solution = solution;
	}

	public String getName() {
		return name;
	}

	public void setQuestion(String name) {
		this.name = name;
	}

	public String[] getAnswers() {
		return answers;
	}

	public void setAnswers(String[] answers) {
		this.answers = answers;
	}

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}
}
